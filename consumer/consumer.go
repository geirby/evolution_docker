package main

import (
	"context"
	"encoding/binary"
	"fmt"
	"github.com/segmentio/kafka-go"
	"log"
	"os"
	"strconv"
	"time"
)

type Config struct {
	brokerAddress string
	topic string
	topic_output string
}

func (cfg *Config) setupEnv() {
	var exists bool
	if cfg.brokerAddress, exists = os.LookupEnv("KAFKA_ADDR"); ! exists {
		cfg.brokerAddress = "localhost:9092"
	}
	if cfg.topic, exists = os.LookupEnv("KAFKA_TOPIC"); ! exists {
		cfg.topic = "input"
	}
	if cfg.topic_output, exists = os.LookupEnv("KAFKA_TOPIC_OUTPUT"); ! exists {
		cfg.topic_output = "output"
	}
}

func main() {
	// create a new context
	cfg := &Config{}
	cfg.setupEnv()
	ctx := context.Background()
	consume(ctx, cfg)
}

func consume(ctx context.Context, cfg *Config) {

	l := log.New(os.Stdout, "kafka consumer reader v0.1: ", 0)
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{cfg.brokerAddress},
		Topic:   cfg.topic,
		Logger: l,
	})
	i := 0
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{cfg.brokerAddress},
		Topic:   cfg.topic_output,
		// assign the logger to the writer
		Logger: l,
	})
	for {
		// the `ReadMessage` method blocks until we receive the next event
		msg, err := r.ReadMessage(ctx)
		if err != nil {
			panic("could not read message " + err.Error())
		}
		// after receiving the message, log its value
		t := time.Unix(int64(binary.LittleEndian.Uint64(msg.Value)), 0).Format(time.RFC3339)

		fmt.Println("received time in RFC3339 format: ", string(t))

		errW := w.WriteMessages(ctx, kafka.Message{
			Key: []byte(strconv.Itoa(i)),
			Value: []byte("this is message " + strconv.Itoa(i) + " --- " + string(t)),
		})
		if errW != nil {
			panic("could not write message " + err.Error())
		}
		i++
	}
}
