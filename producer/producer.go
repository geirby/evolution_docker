package main

import (
	"context"
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/segmentio/kafka-go"
)

type Config struct {
	brokerAddress string
	topic         string
}

func (cfg *Config) setupEnv() {
	var exists bool
	if cfg.brokerAddress, exists = os.LookupEnv("KAFKA_ADDR"); !exists {
		cfg.brokerAddress = "localhost:9092"
	}
	if cfg.topic, exists = os.LookupEnv("KAFKA_TOPIC"); !exists {
		cfg.topic = "input"
	}
}

func main() {
	// create a new context
	cfg := &Config{}
	cfg.setupEnv()
	ctx := context.Background()
	produce(ctx, cfg)
}

func produce(ctx context.Context, cfg *Config) {

	i := 0
	l := log.New(os.Stdout, "kafka v0.4 writer: ", 0)

	// intialize the writer with the broker addresses, and the topic
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{cfg.brokerAddress},
		Topic:   cfg.topic,
		// assign the logger to the writer
		Logger: l,
	})
	b := make([]byte, 8)
	for {

		binary.LittleEndian.PutUint64(b, uint64(time.Now().Unix()))
		err := w.WriteMessages(ctx, kafka.Message{
			Key:   []byte(strconv.Itoa(i)),
			Value: b,
		})
		if err != nil {
			panic("could not write message v0.4 " + err.Error())
		}
		// log a confirmation once the message is written
		fmt.Println("writes:", i)
		i++
		// sleep for a second
		time.Sleep(time.Second)
	}
}
